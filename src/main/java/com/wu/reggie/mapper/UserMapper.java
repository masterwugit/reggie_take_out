package com.wu.reggie.mapper;

import com.wu.reggie.entity.User;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Select;

/**
 * @Author: wu
 * @Date: 2022/6/4 - 06 - 04
 * @Description: com.wu.reggie.mapper
 * @version: 1.0
 */
public interface UserMapper {
    /**
     * 通过手机号查询用户对象
     */
    @Select("SELECT * FROM `user` WHERE phone=#{phone}")
    User findUserByPhone(String phone);

    /**
     * 添加用户和手机，返回主键
     */
    @Insert("insert into user(phone,status) values(#{phone},#{status})")
    @Options(useGeneratedKeys = true, keyProperty = "id", keyColumn = "id")
    void save(User user);

    /**
     * 通过Id查询用户信息
     * @param id
     * @return
     */
    @Select("select * from user where id=#{id}")
    User findById(Long id);
}
