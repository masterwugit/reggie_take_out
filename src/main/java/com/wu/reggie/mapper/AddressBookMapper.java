package com.wu.reggie.mapper;

import com.wu.reggie.entity.AddressBook;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/4 - 06 - 04
 * @Description: com.wu.reggie.mapper
 * @version: 1.0
 */
@Service
public interface AddressBookMapper {
    /**
     * 添加地址
     * @param addressBook
     */
    @Insert("insert into address_book(user_id,consignee,phone,sex,detail,label,create_time,update_time,create_user,update_user) values(#{userId},#{consignee},#{phone},#{sex},#{detail},#{label},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void save(AddressBook addressBook);
    /**
     * 查询某个用户的所有地址列表，按更新时间的降序排序
     * @param userId 用户id
     */
    @Select("select * from address_book where user_id=#{userId} order by update_time desc")
    List<AddressBook> findByUserId(Long userId);
    /**
     * 将某个用户的地址的default修改为0
     * @param userId
     */
    @Update("update address_book set is_default=0 where user_id=#{userId}")
    void removeDefaultAddress(Long userId);
    /**
     * 设置某个地址为默认地址
     * @param addressBook
     */
    @Update("update address_book set is_default=1 where id=#{id}")
    void updateDefaultAddress(AddressBook addressBook);
    /**
     * 通过id删除一条地址
     * @param id
     */
    @Delete("delete from address_book where id=#{id}")
    void deleteById(Long id);
    /**
     * 通过id查询1个地址
     * @param id
     * @return
     */
    @Select("select * from address_book where id=#{id}")
    AddressBook getById(Long id);
    /**
     * 通过id更新所有地址信息
     * @param addressBook
     */
    @Update("UPDATE address_book SET user_id = #{userId},consignee = #{consignee},sex = #{sex},phone = #{phone},province_code = #{provinceCode},province_name = #{provinceName},city_code = #{cityCode},city_name = #{cityName},district_code = #{districtCode},district_name = #{districtName},detail = #{detail},label = #{label},is_default = #{isDefault},create_time = #{createTime},update_time = #{updateTime},create_user = #{createUser},update_user = #{updateUser} WHERE id = #{id}")
    void updateAddress(AddressBook addressBook);
    /**
     * 查询默认地址
     */
    @Select("select * from address_book where user_id=#{userId} and is_default=1")
    AddressBook getDefault(Long userId);
}
