package com.wu.reggie.mapper;

import com.wu.reggie.dto.DishDto;
import com.wu.reggie.entity.Dish;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/5/30 - 05 - 30
 * @Description: com.wu.reggie.mapper
 * @version: 1.0
 */
public interface DishMapper {
    /**
     * 查询某个分类下菜品的个数
     */
    @Select("select count(*) from dish where category_id=#{categoryId}")
    Long findById(Long categoryId);
    /**
     * 添加菜品
     */
    @Insert("INSERT INTO dish VALUES (null,#{name},#{categoryId},#{price},#{code},#{image},#{description},#{status},#{sort},#{createTime},#{updateTime},#{createUser},#{updateUser},0)")
    @Options(useGeneratedKeys = true,keyProperty = "id",keyColumn = "id")
    void save(DishDto dishDto);
    /**
     * 分页查询菜品
     */
    List<DishDto> findPage(@Param("name") String name);
    /**
     * 通过id查询一个菜品
     */
    @Select("select * from dish where id=#{id}")
    Dish findId(Long id);
    /**
     * 更新菜品的信息
     */
    void updateById(DishDto dishDto);
    /**
     * 通过Id删除菜品
     */
    @Delete("delete from dish where id=#{id}")
    void delete(Long id);
    /**
     * 修改status状态
     */
    @Update("update dish set status=#{status} where id=#{id}")
    void updateByStatus(Integer status,Long id);

    /**
     * 通过categoryId查询菜品
     * @param categoryId
     * @return
     */
    @Select("select * from dish where category_id=#{categoryId}")
    List<Dish> findByCategoryId(Long categoryId);

    /**
     * 通过id查询菜品和口味并且是出售状态
     * @param categoryId
     * @param status
     * @return
     */
    List<DishDto> findDishWithFlavor(@Param("categoryId") Long categoryId,@Param("status") Integer status);
}
