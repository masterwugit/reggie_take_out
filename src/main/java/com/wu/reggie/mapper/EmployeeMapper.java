package com.wu.reggie.mapper;

import com.wu.reggie.entity.Category;
import com.wu.reggie.entity.Employee;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Mapper;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/5/27 - 05 - 27
 * @Description: com.wu.reggie.mapper
 * @version: 1.0
 */
public interface EmployeeMapper {
    //通过用户名查询
    @Select("select * from employee where username=#{username}")
    Employee findByUserName(Employee employee);
    //添加员工
    @Insert("insert into employee VALUES (null,#{name},#{username},#{password},#{phone},#{sex},#{idNumber}," +
            "#{status},#{createTime},#{updateTime},#{createUser},#{updateUser})")
    void save(Employee employee);
    //通过姓名查询员工
    List<Employee> findByName(@Param("name") String name);
    //修改员工信息
    void update(Employee employee);
    //通过Id查询员工
    @Select("select * from employee where id=#{id}")
    Employee findById(Long id);
}
