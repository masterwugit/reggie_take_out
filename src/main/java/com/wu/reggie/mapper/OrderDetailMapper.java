package com.wu.reggie.mapper;

import com.wu.reggie.entity.OrderDetail;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/5 - 06 - 05
 * @Description: com.wu.reggie.mapper
 * @version: 1.0
 */
public interface OrderDetailMapper {
    /**
     * 插入多条订单明细数据
     */
    int saveBatch(@Param("orderDetails") List<OrderDetail> orderDetails);
}
