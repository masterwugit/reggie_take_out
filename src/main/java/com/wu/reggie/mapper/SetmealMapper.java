package com.wu.reggie.mapper;

import com.wu.reggie.dto.SetmealDto;
import com.wu.reggie.entity.Setmeal;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Options;
import org.apache.ibatis.annotations.Param;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/5/30 - 05 - 30
 * @Description: com.wu.reggie.mapper
 * @version: 1.0
 */
public interface SetmealMapper {
    /**
     * 统计某个分类下套餐数
     * @param categoryId
     * @return
     */
    @Select("select count(*) from setmeal where category_id=#{categoryId}")
    Long findById(Long categoryId);
    /**
     * 1.添加套餐信息
     * 2.获取自主生成的键
     * @param setmealDto
     */
    @Insert("insert into setmeal values (null,#{categoryId},#{name},#{price},#{status},#{code},#{description},#{image},#{createTime},#{updateTime},#{createUser},#{updateUser},0)")
    @Options(useGeneratedKeys = true,keyProperty = "id",keyColumn = "id")
    void save( SetmealDto setmealDto);

    /**
     * 通过name实现模糊查询套餐和套餐分类
     * @param name
     * @return
     */
    List<SetmealDto> findPage(@Param("name") String name);

    /**
     * 根据ids删除多条数据
     * @param ids
     */
    void delete(@Param("ids") Long[] ids);

    /**
     * 根据ids判断状态为1的个数
     * @param ids
     * @return
     */
    Long queryDishWithStatus(@Param("ids") Long[] ids);

    /**
     * 批量启售,停售
     * @param status
     * @param ids
     */
    void update(@Param("status") int status,@Param("ids") Long[] ids);
    /**
     * 通过id查询套餐并且是出售状态
     * @param categoryId
     * @param status
     * @return
     */
    @Select("select * from setmeal where category_id=#{categoryId} AND status=#{status}")
    List<Setmeal> findSetmealByStatus(@Param("categoryId")Long categoryId,@Param("status")Integer status);
}
