package com.wu.reggie.mapper;

import com.wu.reggie.entity.ShoppingCart;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;
import org.apache.ibatis.annotations.Update;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/4 - 06 - 04
 * @Description: com.wu.reggie.mapper
 * @version: 1.0
 */
public interface ShoppingCartMapper {
    /**
     * 通过菜品id或套餐的id，查询某个用户是否有这一个购物项
     */
    ShoppingCart findCartItem(ShoppingCart shoppingCart);
    /**
     * 添加购物车
     * @param shoppingCart
     */
    @Insert("insert into shopping_cart values (null,#{name},#{image},#{userId},#{dishId},#{setmealId},#{dishFlavor},#{number},#{amount},#{createTime})")
    void save(ShoppingCart shoppingCart);
    /**
     * 通过id更新购物车数量
     */
    @Update("UPDATE shopping_cart SET number=#{number} WHERE id=#{id}")
    void UpdateNumber(ShoppingCart shoppingCart);
    /**
     * 通过UserId查询购物车
     */
    @Select("select * from shopping_cart where user_id=#{userId}")
    List<ShoppingCart> findByUserId(Long userId);
    /**
     * 通过id删除购物车
     */
    @Delete("delete from shopping_cart where id=#{id}")
    void deleteById(Long id);
    /**
     * 通过某个用户的id删除购物车项
     */
    @Delete("delete from shopping_cart where user_id=#{userId}")
    void deleteByUserId(Long userId);

}
