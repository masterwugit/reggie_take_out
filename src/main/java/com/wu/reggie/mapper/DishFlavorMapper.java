package com.wu.reggie.mapper;

import com.wu.reggie.entity.DishFlavor;
import org.apache.ibatis.annotations.Delete;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/1 - 06 - 01
 * @Description: com.wu.reggie.mapper
 * @version: 1.0
 */
public interface DishFlavorMapper {
    /**
     * 添加口味
     */
    @Insert("insert into dish_flavor values (null,#{dishId},#{name},#{value},#{createTime},#{updateTime},#{createUser},#{updateUser},0)")
    void save(DishFlavor dishFlavor);
    /**
     * 通过disId查询口味
     */
    @Select("select * from dish_flavor where dish_id=#{dishId}")
    List<DishFlavor> findByDishId(Long dishId);
    /**
     * 删除某个菜品对应的口味
     */
    @Delete("delete from dish_flavor where dish_id=#{dishId} ")
    void deleteByDishId(Long dishId);
}
