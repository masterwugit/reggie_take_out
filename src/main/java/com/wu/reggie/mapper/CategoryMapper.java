package com.wu.reggie.mapper;

import com.wu.reggie.entity.Category;
import org.apache.ibatis.annotations.*;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/5/30 - 05 - 30
 * @Description: com.wu.reggie.mapper
 * @version: 1.0
 */
public interface CategoryMapper {
    //添加菜品分类
    @Insert("insert into category values ( null, #{type}, #{name}, #{sort}, #{createTime}, #{updateTime}, #{createUser}, #{updateUser})")
    void save(Category category);
    //查询菜品并排序
    @Select("select * from category order by sort")
    List<Category> findAll();
    //删除菜品分类
    @Delete("delete from category where id=#{id}")
    void delete(Long id);
    //更新菜品分类信息
    @Update("update category set name = #{name},sort = #{sort},update_time=#{updateTime},update_user=#{updateUser} WHERE id=#{id} ")
    void update(Category category);
    //查询菜品分类或者套餐
    List<Category> list(@Param("type") Integer type);
}
