package com.wu.reggie.mapper;

import com.wu.reggie.entity.Orders;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/5 - 06 - 05
 * @Description: com.wu.reggie.mapper
 * @version: 1.0
 */
public interface OrderMapper {
    /**
     * 订单的id是生成的，不是自动增长
     * @param orders
     */
    @Insert("insert into orders values(#{id},#{number},#{status},#{userId},#{addressBookId},#{orderTime},#{checkoutTime},#{payMethod},#{amount},#{remark},#{phone},#{address},#{userName},#{consignee})")
    void save(Orders orders);
    /**
     * 查询订单信息
     * @return
     */
    List<Orders> findPage(@Param("number") Integer number, @Param("beginTime") String beginTime, @Param("endTime") String endTime);
}
