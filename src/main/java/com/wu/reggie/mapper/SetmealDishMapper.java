package com.wu.reggie.mapper;

import com.wu.reggie.entity.SetmealDish;
import org.apache.ibatis.annotations.Insert;
import org.apache.ibatis.annotations.Param;

/**
 * @Author: wu
 * @Date: 2022/6/2 - 06 - 02
 * @Description: com.wu.reggie.mapper
 * @version: 1.0
 */
public interface SetmealDishMapper {
    /**
     * 添加1条菜品的数据
     * is_deleted设置为0
     */
    @Insert("insert into setmeal_dish values (null,#{setmealId},#{dishId},#{name},#{price},#{copies},#{sort},#{createTime},#{updateTime},#{createUser},#{updateUser},0)")
    void save(SetmealDish setmealDish);

    /**
     * 根据setmealId数组删除多条数据
     * @param ids
     */
    void delete(@Param("ids") Long[] ids);
}
