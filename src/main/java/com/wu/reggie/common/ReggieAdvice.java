package com.wu.reggie.common;

import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.AfterReturning;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Component;

import javax.servlet.http.HttpSession;
import java.lang.reflect.Method;
import java.time.LocalDateTime;
import java.util.Set;

/**
 * 瑞吉切面类
 */
@Component  //放到容器中
@Aspect  //切面类
@Slf4j  //记录日志
public class ReggieAdvice {
    @Autowired
    private HttpSession session;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 所有保存的方法前调用
     * 通过反射设置四个属性
     */
    @SneakyThrows
    @Before("execution(* com.wu.reggie.service.*Service.save*(*))")  //所有save开头的方法
    public void beforeSave(JoinPoint joinPoint) {
        //从会话域中获取employee的id
        Long id = (Long) session.getAttribute("LOGIN_ID");
        //1. 得到方法的参数数组
        Object[] args = joinPoint.getArgs();
        //2.获取第1个参数，如：employee对象
        Object entity =  args[0];
        //3. 获取参数的类对象
        Class<?> entityClass = entity.getClass();
        //5. 通过反射获取公共的set方法赋值
        //5.1. 设置添加者id的属性
        Method setCreateUser = entityClass.getMethod("setCreateUser", Long.class);
        setCreateUser.invoke(entity,id);
        //5.2. 设置更新者的id属性
        Method setUpdateUser = entityClass.getMethod("setUpdateUser", Long.class);
        setUpdateUser.invoke(entity,id);
        //5.3. 设置创建的时间
        Method setCreateTime = entityClass.getMethod("setCreateTime", LocalDateTime.class);
        setCreateTime.invoke(entity,LocalDateTime.now());
        //5.4. 设置更新的时间
        Method setUpdateTime = entityClass.getMethod("setUpdateTime", LocalDateTime.class);
        setUpdateTime.invoke(entity,LocalDateTime.now());
        log.info("给对象赋值: {}",entity);
    }

    /**
     * 在修改前设置通用2个属性：操作的用户，更新时间
     */
    @SneakyThrows
    @Before("execution(* com.wu.reggie.service.*Service.update*(*))")  //所有update开头的方法
    public void beforeUpdate(JoinPoint joinPoint) {
        //1. 得到方法的参数数组
        Object[] args = joinPoint.getArgs();
        //2. 获取第1个参数，如：employee对象
        Object entity = args[0];
        //3. 获取参数的类对象
        Class<?> entityClass = entity.getClass();
        //4. 从会话域中获取employee的id
        Long id = (Long) session.getAttribute("LOGIN_ID");
        //5. 通过反射获取公共的set方法赋值
        //5.1. 设置更新者的id属性
        Method setUpdateUser = entityClass.getMethod("setUpdateUser", Long.class);
        setUpdateUser.invoke(entity, id);
        //5.2. 设置更新的时间
        Method setUpdateTime = entityClass.getMethod("setUpdateTime", LocalDateTime.class);
        setUpdateTime.invoke(entity, LocalDateTime.now());
        //6. 输出日志信息
        log.info("给对象赋值：{}", entity);
    }
    @AfterReturning("execution(* com..DishService.save*(*)) || execution(* com..DishService.update*(*)) || execution(* com..DishService.delete*(*))")
    public void cleanDish(){
        Set<String> keys = redisTemplate.keys("dish_*");
        redisTemplate.delete(keys);
    }
}
