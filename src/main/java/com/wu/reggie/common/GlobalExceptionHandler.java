package com.wu.reggie.common;

import com.wu.reggie.exception.CustomException;
import com.wu.reggie.exception.ManageException;
import lombok.extern.slf4j.Slf4j;
import org.springframework.web.bind.annotation.ExceptionHandler;
import org.springframework.web.bind.annotation.RestControllerAdvice;

import java.sql.SQLIntegrityConstraintViolationException;

/**
 * @Author: wu
 * @Date: 2022/5/28 - 05 - 28
 * @Description: com.wu.reggie.common
 * @version: 1.0
 */
/**
 * 全局异常处理类
 */
@RestControllerAdvice
@Slf4j
public class GlobalExceptionHandler {
    /**
     * 处理mysql约束异常
     * 如果出现SQLIntegrityConstraintViolationException异常，返回R对象，转成JSON，发给前端
     */
    @ExceptionHandler(SQLIntegrityConstraintViolationException.class)
    public R<String> handleDuplicateEntry(SQLIntegrityConstraintViolationException exception){
        log.error("出现异常,信息是: "+exception.getMessage());
        return R.error("出现异常,信息是: "+exception.getMessage());
    }
    @ExceptionHandler(CustomException.class)
    public R<String> customException(CustomException ex){
        log.info("出现异常,信息是: "+ex.getMessage());
        return R.error("出现异常,信息是: "+ex.getMessage());
    }
    @ExceptionHandler(ManageException.class)
    public R<String> manageException(ManageException ex){
        log.info("出现异常,信息是: "+ex.getMessage());
        return R.error("出现异常,信息是: "+ex.getMessage());
    }
}
