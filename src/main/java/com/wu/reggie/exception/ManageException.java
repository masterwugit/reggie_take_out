package com.wu.reggie.exception;

/**
 * @Author: wu
 * @Date: 2022/5/30 - 05 - 30
 * @Description: com.wu.reggie.exception
 * @version: 1.0
 */
public class ManageException extends RuntimeException {
    public ManageException(String message) {
        super(message);
    }
}
