package com.wu.reggie.exception;

/**
 * @Author: wu
 * @Date: 2022/5/30 - 05 - 30
 * @Description: com.wu.reggie.exception
 * @version: 1.0
 */
/**
 * 用户自定义异常
 */
public class CustomException extends RuntimeException{
    public CustomException(String message) {
        super(message);
    }
}
