package com.wu.reggie.entity;

import lombok.AllArgsConstructor;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.util.List;

@Data
@NoArgsConstructor
@AllArgsConstructor
public class Page<T>  {
    private List<T> records;
    private long total;
    private long pageSize;
    private long page;
}
