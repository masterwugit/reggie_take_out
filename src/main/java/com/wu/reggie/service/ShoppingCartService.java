package com.wu.reggie.service;

import com.wu.reggie.entity.ShoppingCart;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/4 - 06 - 04
 * @Description: com.wu.reggie.service
 * @version: 1.0
 */
public interface ShoppingCartService {
    /**
     * 添加购物车
     * @param shoppingCart
     */
    ShoppingCart add(ShoppingCart shoppingCart);
    /**
     * 通过UserId查询购物车
     */
    List<ShoppingCart> findByUserId(Long userId);
    /**
     * 减少购物车
     */
    ShoppingCart sub(ShoppingCart shoppingCart);
    /**
     * 通过某个用户的id删除购物车项
     */
    void deleteByUserId(Long userId);
}
