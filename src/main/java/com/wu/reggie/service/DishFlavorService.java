package com.wu.reggie.service;

import com.wu.reggie.entity.DishFlavor;

/**
 * @Author: wu
 * @Date: 2022/6/1 - 06 - 01
 * @Description: com.wu.reggie.service
 * @version: 1.0
 */
public interface DishFlavorService {
    /**
     * 添加口味
     * @param dishFlavor
     */
    void save(DishFlavor dishFlavor);
}
