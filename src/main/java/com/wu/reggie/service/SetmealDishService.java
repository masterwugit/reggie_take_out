package com.wu.reggie.service;


import com.wu.reggie.entity.SetmealDish;
import org.apache.ibatis.annotations.Param;

/**
 * @Author: wu
 * @Date: 2022/6/2 - 06 - 02
 * @Description: com.wu.reggie.service
 * @version: 1.0
 */
public interface SetmealDishService {
    /**
     * 添加菜品关系表
     * @param setmealDish
     */
    void save(SetmealDish setmealDish);
    /**
     * 根据setmealId数组删除多条数据
     * @param ids
     */
    void delete(Long[] ids);
}
