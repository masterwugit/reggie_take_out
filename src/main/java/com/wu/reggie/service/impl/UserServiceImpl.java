package com.wu.reggie.service.impl;

import com.wu.reggie.entity.User;
import com.wu.reggie.mapper.UserMapper;
import com.wu.reggie.service.UserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: wu
 * @Date: 2022/6/4 - 06 - 04
 * @Description: com.wu.reggie.service.impl
 * @version: 1.0
 */
@Service
public class UserServiceImpl implements UserService {
    @Autowired
    private UserMapper userMapper;

    /**
     * 登录或注册
     * @param phone 手机号
     * @return
     */
    @Override
    public User login(String phone) {
        //1.通过手机号查询用户
        User user = userMapper.findUserByPhone(phone);
        //2.不存在就添加一个用户，并且返回用户对象
        if (user == null) {
            user = new User();
            //设置手机号
            user.setPhone(phone);
            //可用状态
            user.setStatus(1);
            //保存用户，返回用户的id
            userMapper.save(user);
        }
        //3.如果用户存在就返回用户对象
        return user;
    }
}
