package com.wu.reggie.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wu.reggie.common.R;
import com.wu.reggie.entity.Employee;
import com.wu.reggie.entity.Page;
import com.wu.reggie.exception.ManageException;
import com.wu.reggie.mapper.EmployeeMapper;
import com.wu.reggie.service.EmployeeService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.util.DigestUtils;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/5/27 - 05 - 27
 * @Description: com.wu.reggie.service.impl
 * @version: 1.0
 */
@Service
public class EmployeeServiceImpl implements EmployeeService {
    @Autowired
    private EmployeeMapper employeeMapper;
    /**
     * 登录的方法
     * @param employee 封装了用户名和密码
     * @return 操作结果
     */
    @Override
    public R<Employee> login(Employee employee) {
        //1.密码md5加密 (调用Spring提供的md5加密工具，将密码字符串转成字节数组)
        String md5 = DigestUtils.md5DigestAsHex(employee.getPassword().getBytes());
        //2.根据用户名查询数据库
        Employee doEmployee = employeeMapper.findByUserName(employee);
        //3.如果为空：用户名不存在
        if (doEmployee == null) {
            return R.error("用户名不存在");
        }
        if (!doEmployee.getPassword().equals(md5)) {
            return R.error("密码错误");
        }
        if (doEmployee.getStatus() == 0) {
            return R.error("该员工已经被禁用");
        }
        return R.success(doEmployee);
    }
    //添加员工
    @Override
    public void save(Employee employee) {
        employee.setStatus(1);
//        employee.setCreateTime(LocalDateTime.now());
//        employee.setUpdateTime(LocalDateTime.now());
        String md5 = DigestUtils.md5DigestAsHex("123456".getBytes());
        employee.setPassword(md5);
        employeeMapper.save(employee);
    }
    //分页查询
    @Override
    public Page<Employee> findByName(Integer pageNum, Integer pagSize, String name) {
        PageHelper.startPage(pageNum,pagSize);
        List<Employee> employees = employeeMapper.findByName(name);
        PageInfo<Employee> pageInfo = new PageInfo<>(employees);
        List<Employee> records = pageInfo.getList();
        long total = pageInfo.getTotal();
        int pageSize = pageInfo.getPageSize();
        int page = pageInfo.getPageNum();
        return new Page<>(records,total,pageSize,page);
    }
    //修改员工信息
    @Override
    public void update(Employee employee) {
        if (employee.getId()==1){
            throw new ManageException("管理员信息不可更改");
        }
        employeeMapper.update(employee);
    }
    //通过Id查询员工信息
    @Override
    public Employee findById(Long id) {
        Employee employee = employeeMapper.findById(id);
        return employee;
    }
}
