package com.wu.reggie.service.impl;

import com.wu.reggie.entity.DishFlavor;
import com.wu.reggie.mapper.DishFlavorMapper;
import com.wu.reggie.service.DishFlavorService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: wu
 * @Date: 2022/6/1 - 06 - 01
 * @Description: com.wu.reggie.service.impl
 * @version: 1.0
 */
@Service
public class DishFlavorMapperImpl implements DishFlavorService {
    @Autowired
    private DishFlavorMapper dishFlavorMapper;
    /**
     * 添加口味
     * @param dishFlavor
     */
    @Override
    public void save(DishFlavor dishFlavor) {
        dishFlavorMapper.save(dishFlavor);
    }
}
