package com.wu.reggie.service.impl;

import com.wu.reggie.entity.ShoppingCart;
import com.wu.reggie.mapper.ShoppingCartMapper;
import com.wu.reggie.service.ShoppingCartService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.time.LocalDateTime;
import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/4 - 06 - 04
 * @Description: com.wu.reggie.service.impl
 * @version: 1.0
 */
@Service
public class ShoppingCartServiceImpl implements ShoppingCartService {
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;
    /**
     * 添加购物车
     * @param cart
     */
    @Override
    public ShoppingCart add(ShoppingCart cart) {
        //1. 根据用户的id与dish_id 或者setmeal_id查看购物车表是否存在该购物项
        ShoppingCart cartItem = shoppingCartMapper.findCartItem(cart);
        //2. 如果查询是空，新插入一条记录。
        if (cartItem == null) {
            //对象指向参数对象
            cartItem = cart;
            //2.1. 设置number为1
            cartItem.setNumber(1);
            //2.2. 设置创建的时间
            cartItem.setCreateTime(LocalDateTime.now());
            //2.3. 插入记录并且将查询出来的cart对象设置成参数对象返回
            shoppingCartMapper.save(cartItem);
        }else {
            //3. 如果不为空，数量+1
            cartItem.setNumber(cartItem.getNumber()+1);
            //更新购物车
            shoppingCartMapper.UpdateNumber(cartItem);
        }
        return cartItem;
    }

    /**
     * 通过UserId查询购物车
     * @param userId
     */
    @Override
    public List<ShoppingCart> findByUserId(Long userId) {
        return shoppingCartMapper.findByUserId(userId);
    }

    /**
     * 减少购物车
     * @param shoppingCart
     */
    @Override
    @Transactional
    public ShoppingCart sub(ShoppingCart shoppingCart) {
        ShoppingCart cartItem = shoppingCartMapper.findCartItem(shoppingCart);
        if (cartItem.getNumber() >1) {
            cartItem.setNumber(cartItem.getNumber()-1);
            shoppingCartMapper.UpdateNumber(cartItem);
        }else {
            cartItem.setNumber(0);
            shoppingCartMapper.deleteById(cartItem.getId());
        }
        return cartItem;
    }

    /**
     * 通过某个用户的id删除购物车项
     * @param userId
     */
    @Override
    public void deleteByUserId(Long userId) {
        shoppingCartMapper.deleteByUserId(userId);
    }
}
