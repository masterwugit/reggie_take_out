package com.wu.reggie.service.impl;

import com.wu.reggie.entity.AddressBook;
import com.wu.reggie.mapper.AddressBookMapper;
import com.wu.reggie.service.AddressBookService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/4 - 06 - 04
 * @Description: com.wu.reggie.service.impl
 * @version: 1.0
 */
@Service
public class AddressBookServiceImpl implements AddressBookService {
    @Autowired
    private AddressBookMapper addressBookMapper;
    /**
     * 添加地址
     * @param addressBook
     */
    @Override
    public void save(AddressBook addressBook) {
        addressBookMapper.save(addressBook);
    }

    /**
     * 查询某个用户的所有地址列表，按更新时间的降序排序
     * @param userId 用户id
     */
    @Override
    public List<AddressBook> findByUserId(Long userId) {
        return addressBookMapper.findByUserId(userId);
    }

    /**
     * 设置某个地址为默认地址
     * @param addressBook
     */
    @Override
    @Transactional
    public void updateDefaultAddress(AddressBook addressBook) {
        addressBookMapper.removeDefaultAddress(addressBook.getUserId());
        addressBookMapper.updateDefaultAddress(addressBook);
    }
    /**
     * 通过id删除一条地址
     * @param id
     */
    @Override
    public void deleteById(Long id) {
        addressBookMapper.deleteById(id);

    }

    /**
     * 通过id查询1个地址
     * @param id
     * @return
     */
    @Override
    public AddressBook getById(Long id) {
        return addressBookMapper.getById(id);
    }

    /**
     * 通过id更新所有地址信息
     * @param addressBook
     */
    @Override
    public void updateAddress(AddressBook addressBook) {
        addressBookMapper.updateAddress(addressBook);
    }

    /**
     * 查询默认地址
     * @param userId
     */
    @Override
    public AddressBook getDefault(Long userId) {
        return addressBookMapper.getDefault(userId);
    }
}
