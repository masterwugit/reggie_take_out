package com.wu.reggie.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wu.reggie.entity.Category;
import com.wu.reggie.entity.Page;
import com.wu.reggie.exception.CustomException;
import com.wu.reggie.mapper.CategoryMapper;
import com.wu.reggie.mapper.DishMapper;
import com.wu.reggie.mapper.SetmealMapper;
import com.wu.reggie.service.CategoryService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/5/30 - 05 - 30
 * @Description: com.wu.reggie.service.impl
 * @version: 1.0
 */
@Service
@Slf4j
public class CategoryServiceImpl implements CategoryService {
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private CategoryMapper categoryMapper;
    @Override
    public void save(Category category) {
        categoryMapper.save(category);
    }
    //分页查询菜品
    @Override
    public Page<Category> findAllByPage(Integer pageNum, Integer pageSize) {
        PageHelper.startPage(pageNum,pageSize);
        List<Category> categories = categoryMapper.findAll();
        PageInfo<Category> pageInfo = new PageInfo<>(categories);
        return new Page<>(pageInfo.getList(),pageInfo.getTotal(),pageSize,pageNum);
    }
    //根据id删除菜品分类
    @Override
    public void deleteById(Long id) {
        log.info("进入");
        Long count = dishMapper.findById(id);
        if (count > 0) {
            log.info("异常");
            throw new CustomException("该分类下有菜品,不可以删除");
        }
        count = setmealMapper.findById(id);
        if (count > 0) {
            throw new CustomException("该分类下有套餐,不可以删除");
        }
        categoryMapper.delete(id);
    }
    //更新菜品分类信息
    @Override
    public void update(Category category) {
        categoryMapper.update(category);
    }
    //查询所有的菜品分类信息并导出
    @Override
    public List<Category> findAll() {
        return categoryMapper.findAll();
    }
    //查询菜品分类或者套餐
    @Override
    public List<Category> list(Integer type) {
        return categoryMapper.list(type);
    }


}
