package com.wu.reggie.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wu.reggie.entity.*;
import com.wu.reggie.mapper.*;
import com.wu.reggie.service.OrderService;
import com.wu.reggie.util.UUIDUtils;
import org.apache.poi.ooxml.util.PackageHelper;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.math.BigDecimal;
import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/5 - 06 - 05
 * @Description: com.wu.reggie.service.impl
 * @version: 1.0
 */
@Service
public class OrderServiceImpl implements OrderService {
    @Autowired
    private OrderMapper orderMapper;  //订单
    @Autowired
    private OrderDetailMapper orderDetailMapper;  //订单明细
    @Autowired
    private ShoppingCartMapper shoppingCartMapper;  //购物车
    @Autowired
    private UserMapper userMapper;  //用户
    @Autowired
    private AddressBookMapper addressBookMapper;  //地址
    /**
     * 作用： 下单
     * @param orders 订单的信息（地址id，支付方式，备注）
     * @param userId
     * @return
     * @Param userId  当前的登陆者
     */
    @Override
    @Transactional
    public void submit(Orders orders, Long userId) {
        //1. 通过用户id，查询当前用户的购物车数据
        List<ShoppingCart> carts = shoppingCartMapper.findByUserId(userId);
        //2. 根据当前登录用户id, 查询用户数据 (需要创建持久层方法)
        User user = userMapper.findById(userId);
        //3. 根据订单地址ID, 查询地址数据
        AddressBook addressBook = addressBookMapper.getById(orders.getAddressBookId());
        //4. 通过工具类生成uuid的订单id (在资料中提供了工具类)
        long orderId = UUIDUtils.getUUIDInOrderId();
        //5. 声明变量记录总金额，供后面使用
        BigDecimal totalAmount = BigDecimal.ZERO;
        //6. 声明变量为0，用于统计共有多少件商品
        int totalNumber = 0;
        //7. 创建一个订单项集合
        List<OrderDetail> orderDetails = new ArrayList<>();
        //8. 遍历所有的购物项，一个购物项就是一个订单明细
        for (ShoppingCart cart : carts) {
            //8.1. 创建订单明细对象
            OrderDetail detail = new OrderDetail();
            //8.2 购物车与订单明细表中相同的属性可以直接复制
            BeanUtils.copyProperties(cart, detail);
            //8.3. 设置订单项目的主键，使用uuid工具类生成
            detail.setId(UUIDUtils.getUUIDInOrderId().longValue());
            //8.4. 订单项所属的订单的id(上面生成的订单id)
            detail.setOrderId(orderId);
            //8.5  累加总数量
            totalNumber += cart.getNumber();
            //8.6. 计算当前购物项的总价：数量乘以单价
            BigDecimal price = cart.getAmount().multiply(BigDecimal.valueOf(cart.getNumber()));
            //8.7. 再累加到上面定义的总价变量中
            totalAmount = totalAmount.add(price);
            //8.14. 添加到购物车项集合中
            orderDetails.add(detail);
        }
        //9. 组装订单数据, 使用方法的参数orders
        //9.1. 设置订单号
        orders.setId(orderId);
        //9.2. 设置商品的数量
        orders.setNumber(String.valueOf(totalNumber));
        //9.3. 设置状态码为1
        orders.setStatus(1);
        //9.4. 设置用户id
        orders.setUserId(userId);
        //9.5. 设置下单时间为现在
        orders.setOrderTime(LocalDateTime.now());
        //9.6. 设置支付时间为现在时间加30分钟
        orders.setCheckoutTime(LocalDateTime.now().plusMinutes(30));
        //9.7. 设置总金额
        orders.setAmount(totalAmount);
        //9.8. 设置登录用户名
        orders.setUserName(user.getName());
        //9.9. 设置收货电话
        orders.setPhone(addressBook.getPhone());
        //9.10. 设置收货地址
        orders.setAddress(addressBook.getDetail());
        //9.11. 收货人consignee
        orders.setConsignee(addressBook.getConsignee());
        //10. 调用orderMapper插入订单 (编写新的持久层方法)
        orderMapper.save(orders);
        //11. 调用orderDetailMapper批量插入订单项 (编写新的持久层方法)
        orderDetailMapper.saveBatch(orderDetails);
        //12. 删除当前用户的购物车列表数据
        shoppingCartMapper.deleteByUserId(userId);

    }

    /**
     * 查询订单信息
     * @param pageNum
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    @Override
    public Page<Orders> findPage(Integer pageNum, Integer pageSize, Integer number, String beginTime, String endTime) {
        PageHelper.startPage(pageNum,pageSize);
        List<Orders> orders = orderMapper.findPage(number, beginTime, endTime);
        PageInfo<Orders> pageInfo = new PageInfo<>(orders);
        return new Page<>(pageInfo.getList(),pageInfo.getTotal(),pageSize,pageNum);
    }
}
