package com.wu.reggie.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wu.reggie.dto.SetmealDto;
import com.wu.reggie.entity.Page;
import com.wu.reggie.entity.Setmeal;
import com.wu.reggie.entity.SetmealDish;
import com.wu.reggie.exception.CustomException;
import com.wu.reggie.mapper.SetmealDishMapper;
import com.wu.reggie.mapper.SetmealMapper;
import com.wu.reggie.service.SetmealDishService;
import com.wu.reggie.service.SetmealService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.CacheEvict;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/2 - 06 - 02
 * @Description: com.wu.reggie.service.impl
 * @version: 1.0
 */
@Service
public class SetmealServiceImpl implements SetmealService {
    @Autowired
    private SetmealMapper setmealMapper;
    @Autowired
    private SetmealDishService setmealDishService;
    /**
     * 添加套餐和套餐菜品关系表
     * @param setmealDto
     */
    @Override
    @Transactional
    //allEntries = true 删除这个名字下所有的键
    @CacheEvict(cacheNames = "setmeal" ,allEntries = true)
    public void saveWithDish(SetmealDto setmealDto) {
        setmealMapper.save(setmealDto);
        List<SetmealDish> dishes = setmealDto.getSetmealDishes();
        dishes.forEach(setmealDish -> {
            setmealDish.setSetmealId(setmealDto.getId());
            setmealDishService.save(setmealDish);
        });
    }
    /**
     * 分页查询
     * @param pageNum
     * @param pageSize
     * @param name
     * @return
     */
    @Override
    //@Cacheable(cacheNames = "stemealPage" , key = "#pageNum+'_'+#pageSize+'_'+#name",unless = "#result==null")
    public Page<SetmealDto> findPage(Integer pageNum, Integer pageSize, String name) {
        PageHelper.startPage(pageNum,pageSize);
        List<SetmealDto> page = setmealMapper.findPage(name);
        PageInfo<SetmealDto> pageInfo = new PageInfo<>(page);
        return new Page<>(pageInfo.getList(),pageInfo.getTotal(),pageSize,pageNum);
    }
    /**
     * 根据ids删除多条数据和套餐菜品关系表
     * @param ids
     */
    @Override
    @Transactional
    @CacheEvict(cacheNames = "stemeal",allEntries = true)
    public void delete(Long[] ids) {
        Long status = setmealMapper.queryDishWithStatus(ids);
        if (status > 0) {
            throw new CustomException("该套餐属于出售状态,不可删除");
        }
        setmealMapper.delete(ids);
        setmealDishService.delete(ids);
    }
    /**
     * 批量启售,停售
     * @param status
     * @param ids
     */
    @Override
    @CacheEvict(cacheNames = "stemeal",allEntries = true)
    public void updateStatus(int status, Long[] ids) {
        setmealMapper.update(status,ids);
    }

    /**
     * 通过id查询套餐并且是出售状态
     * @param categoryId
     * @param status
     * @return
     */
    //condition:"#result != null"表示满足什么条件,再进行缓存;
    //unless: "#result==null"为真时不执行;
    @Override
    @Cacheable(cacheNames = "setmeal" ,key ="#categoryId+'_'+#status",unless = "#result==null")
    public List<Setmeal> findSetmealByStatus(Long categoryId, Integer status) {
        return setmealMapper.findSetmealByStatus(categoryId,status);
    }
}
