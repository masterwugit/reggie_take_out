package com.wu.reggie.service.impl;

import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wu.reggie.dto.DishDto;
import com.wu.reggie.entity.Dish;
import com.wu.reggie.entity.DishFlavor;
import com.wu.reggie.entity.Page;
import com.wu.reggie.exception.CustomException;
import com.wu.reggie.mapper.DishFlavorMapper;
import com.wu.reggie.mapper.DishMapper;
import com.wu.reggie.service.DishFlavorService;
import com.wu.reggie.service.DishService;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.cache.annotation.Cacheable;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.concurrent.TimeUnit;

/**
 * @Author: wu
 * @Date: 2022/6/1 - 06 - 01
 * @Description: com.wu.reggie.service.impl
 * @version: 1.0
 */
@Service
public class DishServiceImpl implements DishService {
    @Autowired
    private DishMapper dishMapper;
    @Autowired
    private DishFlavorService dishFlavorService;
    @Autowired
    private DishFlavorMapper dishFlavorMapper;
    @Autowired
    private RedisTemplate<String, Object> redisTemplate;

    /**
     * 添加菜品
     * @param dishDto
     */
    @Transactional
    @Override
    public void saveWithFlavor(DishDto dishDto) {
        dishDto.setSort(0);
        dishMapper.save(dishDto);
        List<DishFlavor> flavors = dishDto.getFlavors();
        flavors.forEach(dishFlavor -> {
            dishFlavor.setDishId(dishDto.getId());
            dishFlavorService.save(dishFlavor);
        });
    }

    /**
     * 分页查询菜品
     * @param pageNum
     * @param pageSize
     * @param name
     */
    @Override
    public Page<DishDto> findPage(Integer pageNum, Integer pageSize, String name) {
        PageHelper.startPage(pageNum,pageSize);
        List<DishDto> page = dishMapper.findPage(name);
        PageInfo<DishDto> pageInfo = new PageInfo<>(page);
        return new Page<>(pageInfo.getList(),pageInfo.getTotal(),pageSize,pageNum);
    }

    /**
     * 通过id查询菜品和口味信息
     * @param id
     */
    @Override
    public DishDto findById(Long id) {
        Dish dish = dishMapper.findId(id);
        List<DishFlavor> dishFlavors = dishFlavorMapper.findByDishId(dish.getId());
        DishDto dishDto = new DishDto();
        BeanUtils.copyProperties(dish,dishDto);
        dishDto.setFlavors(dishFlavors);
        return dishDto;
    }
    /**
     * 修改菜品信息
     * @param dishDto
     */
    @Transactional
    @Override
    public void updateById(DishDto dishDto) {
        dishMapper.updateById(dishDto);
        dishFlavorMapper.deleteByDishId(dishDto.getId());
        List<DishFlavor> flavors = dishDto.getFlavors();
        flavors.forEach(dishFlavor -> {
            dishFlavorMapper.save(dishFlavor);
        });
    }

    /**
     * 通过Id删除菜品
     * @param id
     */
    @Override
    @Transactional
    public void deleteById(Long[] ids) {
        for (Long id : ids) {
            Dish dish = dishMapper.findId(id);
            Integer status = dish.getStatus();
            if (status == 1) {
                throw new CustomException("菜品正在使用不可删除");
            }
            dishMapper.delete(id);
            dishFlavorMapper.deleteByDishId(dish.getId());
        }
    }
    /**
     * 修改status状态
     * @param status
     * @param ids
     */
    @Override
    public void updateByStatus(Integer status, Long[] ids) {
        for (Long id : ids) {
            dishMapper.updateByStatus(status,id);
        }
    }

    /**
     * 通过categoryId查询菜品
     * @param categoryId
     * @return
     */
    @Override
    public List<Dish> findByCategoryId(Long categoryId) {
        List<Dish> dishes = dishMapper.findByCategoryId(categoryId);
        return dishes;
    }

    /**
     * 通过id查询菜品和口味并且是出售状态
     * @param categoryId
     * @param status
     * @return
     */
    @Override
    //@Cacheable(cacheNames = "dish" , key = "#categoryId+'_'+#status")
    public List<DishDto> findDishWithFlavor(Long categoryId, Integer status) {
        //1.通过key从redis中获取值
        String key = "dish_" +categoryId+"_"+status;
        List<DishDto> dishDtos = (List<DishDto>) redisTemplate.opsForValue().get(key);
        //2.如果为空，则从数据库中查询
        if (dishDtos == null) {
            //2.1. 根据菜品类别的id和状态查找菜品
            dishDtos = dishMapper.findDishWithFlavor(categoryId, status);
            //2.2.并且向redis中保存一份，保留2天
            redisTemplate.opsForValue().set(key,dishDtos,2, TimeUnit.DAYS);
        }
        //3. 返回List<DishDto>集合
        return dishDtos;
    }
}
