package com.wu.reggie.service.impl;

import com.wu.reggie.entity.SetmealDish;
import com.wu.reggie.mapper.SetmealDishMapper;
import com.wu.reggie.service.SetmealDishService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * @Author: wu
 * @Date: 2022/6/2 - 06 - 02
 * @Description: com.wu.reggie.service.impl
 * @version: 1.0
 */
@Service
public class SetmealDishServiceImpl implements SetmealDishService {
    @Autowired
    private SetmealDishMapper setmealDishMapper;
    /**
     * 添加菜品关系表
     * @param setmealDish
     */
    @Override
    public void save(SetmealDish setmealDish) {
        setmealDish.setSort(0);
        setmealDishMapper.save(setmealDish);
    }

    /**
     * 根据setmealId数组删除多条数据
     * @param ids
     */
    @Override
    public void delete(Long[] ids) {
        setmealDishMapper.delete(ids);
    }
}
