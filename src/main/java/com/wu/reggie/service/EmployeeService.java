package com.wu.reggie.service;


import com.wu.reggie.common.R;
import com.wu.reggie.entity.Category;
import com.wu.reggie.entity.Employee;
import com.wu.reggie.entity.Page;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/5/27 - 05 - 27
 * @Description: com.wu.reggie.service
 * @version: 1.0
 */
/**
 * 登录的方法
 * @param employee
 * @return 操作结果
 */
public interface EmployeeService {
    //通过用户名查询
    R<Employee> login(Employee employee);
    //添加员工
    void save(Employee employee);
    //通过姓名查询员工
    Page<Employee> findByName(Integer pageNum,Integer pagSize,String name);
    //修改员工信息
    void update(Employee employee);
    //通过Id查询员工信息
    Employee findById(Long id);



}
