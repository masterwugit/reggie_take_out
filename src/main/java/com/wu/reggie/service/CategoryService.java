package com.wu.reggie.service;

import com.wu.reggie.entity.Category;
import com.wu.reggie.entity.Page;

import java.util.List;


/**
 * @Author: wu
 * @Date: 2022/5/30 - 05 - 30
 * @Description: com.wu.reggie.service
 * @version: 1.0
 */
public interface CategoryService {
    //添加菜品分类
    void save(Category category);
    //分页查询菜品
    Page<Category> findAllByPage(Integer pageNum, Integer pageSize);
    //删除菜品分类
    void deleteById(Long id);
    //更新菜品分类信息
    void update(Category category);
    //查询所有的菜品分类信息并导出
    List<Category> findAll();
    //查询菜品分类或者套餐
    List<Category> list(Integer type);

}
