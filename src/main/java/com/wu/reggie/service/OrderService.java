package com.wu.reggie.service;

import com.wu.reggie.entity.Orders;
import com.wu.reggie.entity.Page;

/**
 * @Author: wu
 * @Date: 2022/6/5 - 06 - 05
 * @Description: com.wu.reggie.service
 * @version: 1.0
 */
public interface OrderService {
    /**
     * 作用： 下单
     * @param orders  订单的信息（地址id，支付方式，备注）
     * @Param userId  当前的登陆者
     * @return
     */
    void submit(Orders orders, Long userId);
    /**
     * 查询订单信息
     * @return
     */
    Page<Orders> findPage(Integer pageNum, Integer pageSize,Integer number, String beginTime, String endTime);

}

