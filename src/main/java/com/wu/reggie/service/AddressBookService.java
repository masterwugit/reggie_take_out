package com.wu.reggie.service;

import com.wu.reggie.entity.AddressBook;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/4 - 06 - 04
 * @Description: com.wu.reggie.service
 * @version: 1.0
 */
public interface AddressBookService {
    /**
     * 添加地址
     * @param addressBook
     */
    void save(AddressBook addressBook);
    /**
     * 查询某个用户的所有地址列表，按更新时间的降序排序
     * @param userId 用户id
     */
    List<AddressBook> findByUserId(Long userId);
    /**
     * 设置某个地址为默认地址
     * @param addressBook
     */
    void updateDefaultAddress(AddressBook addressBook);
    /**
     * 通过id删除一条地址
     * @param id
     */
    void deleteById(Long id);
    /**
     * 通过id查询1个地址
     * @param id
     * @return
     */
    AddressBook getById(Long id);
    /**
     * 通过id更新所有地址信息
     * @param addressBook
     */
    void updateAddress(AddressBook addressBook);
    /**
     * 查询默认地址
     */
    AddressBook getDefault(Long userId);

}
