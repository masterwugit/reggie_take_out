package com.wu.reggie.service;

import com.wu.reggie.dto.DishDto;
import com.wu.reggie.entity.Dish;
import com.wu.reggie.entity.Page;
import org.apache.ibatis.annotations.Select;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/1 - 06 - 01
 * @Description: com.wu.reggie.service
 * @version: 1.0
 */
public interface DishService {
    /**
     * 添加菜品和口味
     */
    void saveWithFlavor(DishDto dishDto);
    /**
     * 分页查询菜品
     */
    Page<DishDto> findPage(Integer pageNum,Integer pageSize ,String name);
    /**
     * 通过id查询菜品和口味信息
     */
    DishDto findById(Long id);
    /**
     * 修改菜品信息
     */
    void updateById(DishDto dishDto);
    /**
     * 通过Id删除菜品
     */
    void deleteById(Long[] ids);
    /**
     * 修改status状态
     */
    void updateByStatus(Integer status,Long[] ids);
    /**
     * 通过categoryId查询菜品
     * @param categoryId
     * @return
     */
    List<Dish> findByCategoryId(Long categoryId);
    /**
     * 通过id查询菜品和口味并且是出售状态
     * @param categoryId
     * @param status
     * @return
     */
    List<DishDto> findDishWithFlavor(Long categoryId,Integer status);
}
