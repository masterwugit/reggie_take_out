package com.wu.reggie.service;

import com.wu.reggie.entity.User;

/**
 * @Author: wu
 * @Date: 2022/6/4 - 06 - 04
 * @Description: com.wu.reggie.service
 * @version: 1.0
 */
public interface UserService {
    /**
     * 登录或注册
     * @param phone 手机号
     * @return
     */
    User login(String phone);
}
