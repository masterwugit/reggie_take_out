package com.wu.reggie.service;


import com.wu.reggie.dto.SetmealDto;
import com.wu.reggie.entity.Page;
import com.wu.reggie.entity.Setmeal;

import java.util.List;


/**
 * @Author: wu
 * @Date: 2022/6/2 - 06 - 02
 * @Description: com.wu.reggie.service
 * @version: 1.0
 */
public interface SetmealService {
    /**
     * 添加套餐和套餐菜品关系表
     * @param setmealDto
     */
    void saveWithDish(SetmealDto setmealDto);

    /**
     * 分页查询
     * @param pageNum
     * @param pageSize
     * @param name
     * @return
     */
   Page<SetmealDto> findPage(Integer pageNum,Integer pageSize,String name);
    /**
     * 根据ids删除多条数据套餐菜品关系表
     * @param ids
     */
    void delete(Long[] ids);
    /**
     * 批量启售,停售
     * @param status
     * @param ids
     */
    void updateStatus(int status,Long[] ids);
    /**
     * 通过id查询套餐并且是出售状态
     * @param categoryId
     * @param status
     * @return
     */
    List<Setmeal> findSetmealByStatus(Long categoryId,Integer status);
}
