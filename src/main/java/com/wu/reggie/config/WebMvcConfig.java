package com.wu.reggie.config;

import com.wu.reggie.common.CheckLoginInterceptor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.converter.HttpMessageConverter;
import org.springframework.http.converter.json.MappingJackson2HttpMessageConverter;
import org.springframework.web.servlet.config.annotation.InterceptorRegistry;
import org.springframework.web.servlet.config.annotation.ResourceHandlerRegistry;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/5/27 - 05 - 27
 * @Description: com.wu.reggie.config
 * @version: 1.0
 */
@Configuration
@Slf4j
public class WebMvcConfig extends WebMvcConfigurationSupport {
    @Override
    protected void addResourceHandlers(ResourceHandlerRegistry registry) {
        log.info("静态资源访问");
        registry.addResourceHandler("/backend/**").addResourceLocations("classpath:/backend/");
        registry.addResourceHandler("/front/**").addResourceLocations("classpath:/front/");
    }
    /**
     * 配置拦截器
     */
    @Override
    protected void addInterceptors(InterceptorRegistry registry) {
        //日志显示拦截器加载
        log.info("加载了用户权限拦截器");
        //不拦截的地址
        String[] urls = {
                "/employee/login",
                "/employee/logout",
                "/backend/**",
                "/front/**",
                "/common/**",
                "/user/sendMsg",  //发短信
                "/user/login"   //手机端登录
        };
        //addPathPatterns: 指定拦截地址  excludePathPatterns：排除哪些地方不拦截
        registry.addInterceptor(new CheckLoginInterceptor()).addPathPatterns("/**").excludePathPatterns(urls);
    }
    /**
     * 扩展消息转换器，使用自定义消息转换
     * 将自定义的消息转换器添加到List集合中，添加到第0个元素，优先处理
     */
    @Override
    protected void extendMessageConverters(List<HttpMessageConverter<?>> converters) {
        //1.创建消息转换器对象
        MappingJackson2HttpMessageConverter converter = new MappingJackson2HttpMessageConverter();
        //2.设置转换器属性
        converter.setObjectMapper(new JacksonObjectMapper());
        //3.将这个消息转换器放在第0个位置
        converters.add(0,converter);
    }
}
