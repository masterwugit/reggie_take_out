package com.wu.reggie.dto;


import com.wu.reggie.entity.Setmeal;
import com.wu.reggie.entity.SetmealDish;
import lombok.Data;

import java.util.List;

@Data
public class SetmealDto extends Setmeal {

    private List<SetmealDish> setmealDishes;  //多个菜品信息

    private String categoryName;  //分类的名字

}
