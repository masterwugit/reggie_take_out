package com.wu.reggie.dto;


import com.wu.reggie.entity.Dish;
import com.wu.reggie.entity.DishFlavor;
import lombok.Data;
import java.util.ArrayList;
import java.util.List;

@Data
public class DishDto extends Dish {
    private List<DishFlavor> flavors = new ArrayList<>();
    private String categoryName;  //所属分类的名字
    private Integer copies;   //份数
}
