package com.wu.reggie.controller;

import com.wu.reggie.common.R;
import com.wu.reggie.entity.Category;
import com.wu.reggie.entity.Page;
import com.wu.reggie.service.CategoryService;
import lombok.SneakyThrows;
import lombok.extern.slf4j.Slf4j;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.InputStream;
import java.util.ArrayList;
import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/5/30 - 05 - 30
 * @Description: com.wu.reggie.controller
 * @version: 1.0
 */
@RestController
@RequestMapping("/category")
@Slf4j
public class CategoryController extends BaseController {
    @Autowired
    private CategoryService categoryService;
    //添加菜品分类
    @PostMapping
    public R<String> save(@RequestBody Category category){
        log.info("添加分类成功");
        categoryService.save(category);
        return R.success("添加成功");
    }
    //分页查询菜品
    @GetMapping("page")
    public R<Page<Category>> findAll(@RequestParam("page") Integer pageNum,Integer pageSize){
        Page<Category> page = categoryService.findAllByPage(pageNum, pageSize);
        return R.success(page);
    }
    //根据id删除菜品分类
    @DeleteMapping
    public R<String> delete(Long id){
        log.info("删除菜品分类");
        categoryService.deleteById(id);
        return R.success("删除成功");
    }
    //更新菜品分类信息
    @PutMapping
    public R<String> update(@RequestBody Category category){
        log.info("修改菜品信息成功");
        categoryService.update(category);
        return R.success("修改成功");
    }
    /**
     * 导出Excel
     */
    @SneakyThrows
    @GetMapping("/exportExcel")
    public void exportExcel(){
        //指定响应类型为excel格式
        response.setContentType("application/vnd.ms-excel");
        //以下载的方式打开，而不是直接在浏览器中打开 (inline在线打开 attachment 下载 filename指定下载的文件名)
        response.setHeader("content-disposition", "attachment;filename=category.xlsx");
        //1.获取模板文件输入流
        InputStream inputStream = getClass().getResourceAsStream("/excel/category_template.xlsx");
        //2.创建工作簿
        XSSFWorkbook workbook = new XSSFWorkbook(inputStream);
        //3.获取工作表
        XSSFSheet sheet = workbook.getSheetAt(0);
        //4.读取第2行的样式，放在集合中
       List<CellStyle> cellStyles = new ArrayList<>();
        Row styleRow = sheet.getRow(1);
        for (int i = 0; i < 5; i++) {
            //获取这一行的单元格对象
            Cell cell = styleRow.getCell(i);
            //读取这一行的样式，并且添加到上面的集合中
            cellStyles.add(cell.getCellStyle());
        }
        //调用业务层获取所有的分类
        List<Category> categories = categoryService.findAll();
        //遍历集合
        for (int i = 0; i < categories.size(); i++) {
            Category category = categories.get(i);
            //创建一行
            Row row = sheet.createRow(i + 1);
            //创建单元格，并且指定样式
            Cell cell = row.createCell(0);
            //设置数据
            cell.setCellValue(category.getType());
            cell.setCellStyle(cellStyles.get(0));

            //创建单元格
            cell = row.createCell(1);
            //设置数据
            cell.setCellValue(category.getName());
            //设置样式
            cell.setCellStyle(cellStyles.get(1));

            //创建单元格
            cell = row.createCell(2);
            //设置数据
            cell.setCellValue(category.getSort());
            //设置样式
            cell.setCellStyle(cellStyles.get(2));

            //创建单元格
            cell = row.createCell(3);
            //设置数据
            cell.setCellValue(category.getCreateTime().toLocalDate().toString());
            //设置样式
            cell.setCellStyle(cellStyles.get(3));

            //创建单元格
            cell = row.createCell(4);
            //设置数据
            cell.setCellValue(category.getUpdateTime().toLocalDate().toString());
            //设置样式
            cell.setCellStyle(cellStyles.get(4));
        }
        //写出到响应的输出流
        workbook.write(response.getOutputStream());
    }
    //查询菜品分类或者套餐
    @GetMapping("list")
    public R<List<Category>> list(Integer type){
        List<Category> categories = categoryService.list(type);
        return R.success(categories);
    }
}
