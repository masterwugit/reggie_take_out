package com.wu.reggie.controller;

import com.wu.reggie.common.R;
import com.wu.reggie.entity.AddressBook;
import com.wu.reggie.service.AddressBookService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/4 - 06 - 04
 * @Description: com.wu.reggie.controller
 * @version: 1.0
 */
@Slf4j
@RestController
@RequestMapping("addressBook")
public class AddressBookController extends BaseController {
    @Autowired
    private AddressBookService addressBookService;
    @PostMapping
    public R<String> add(@RequestBody AddressBook addressBook){
        log.info("添加地址");
        long userId = getLonginIdFromSession();
        addressBook.setUserId(userId);
        addressBookService.save(addressBook);
        return R.success("添加成功");
    }
    /**
     * 查询某个用户的所有地址列表，按更新时间的降序排序
     * @param userId 用户id
     */
    @GetMapping("list")
    public R<List<AddressBook>> findByUserID(){
        long userId = getLonginIdFromSession();
        List<AddressBook> addressBooks = addressBookService.findByUserId(userId);
        return R.success(addressBooks);
    }
    /**
     * 设置某个地址为默认地址
     * @param addressBook
     */
    @PutMapping("default")
    public R<AddressBook> updateDefault(@RequestBody AddressBook addressBook){
        log.info("设置默认地址");
        long userId = getLonginIdFromSession();
        addressBook.setUserId(userId);
        addressBookService.updateDefaultAddress(addressBook);
        return R.success(addressBook);
    }
    /**
     * 通过id删除一条地址
     * @param id
     */
    @DeleteMapping
    public R<String> delete(Long ids){
        log.info("删除地址");
        addressBookService.deleteById(ids);
        return R.success("删除成功");
    }
    /**
     * 通过id查询1个地址
     * @param id
     * @return
     */
    @GetMapping("{id}")
    public R<AddressBook> getById(@PathVariable Long id){
        AddressBook addressBook = addressBookService.getById(id);
        return R.success(addressBook);
    }
    /**
     * 通过id更新所有地址信息
     * @param addressBook
     */
    @PutMapping
    public R<String> updateAddress(@RequestBody AddressBook addressBook){
        log.info("修改收货地址");
        addressBookService.updateAddress(addressBook);
        return R.success("修改成功");
    }
    /**
     * 查询默认地址
     */
    @GetMapping("default")
    public R<AddressBook> getDefault(){
        long userId = getLonginIdFromSession();
        AddressBook addressBook = addressBookService.getDefault(userId);
        if (addressBook == null) {
            return R.error("该对象没有默认地址");
        }
        return R.success(addressBook);
    }
}
