package com.wu.reggie.controller;

import com.wu.reggie.common.R;
import com.wu.reggie.entity.ShoppingCart;
import com.wu.reggie.service.ShoppingCartService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/4 - 06 - 04
 * @Description: com.wu.reggie.controller
 * @version: 1.0
 */
@RestController
@Slf4j
@RequestMapping("shoppingCart")
public class ShoppingCartController extends BaseController {
    @Autowired
    private ShoppingCartService shoppingCartService;
    @PostMapping("add")
    public R<ShoppingCart> add(@RequestBody ShoppingCart shoppingCart){
        long userId = getLonginIdFromSession();
        shoppingCart.setUserId(userId);
        ShoppingCart Cart = shoppingCartService.add(shoppingCart);
        return R.success(Cart);
    }
    /**
     * 通过UserId查询购物车
     */
    @GetMapping("list")
    public R<List<ShoppingCart>> findByUserId(){
        List<ShoppingCart> shoppingCarts = shoppingCartService.findByUserId(getLonginIdFromSession());
        return R.success(shoppingCarts);
    }
    /**
     * 减少购物车
     * @param shoppingCart
     */
    @PostMapping("sub")
    public R<ShoppingCart> sub(@RequestBody ShoppingCart shoppingCart){
        long userId = getLonginIdFromSession();
        shoppingCart.setUserId(userId);
        ShoppingCart cart = shoppingCartService.sub(shoppingCart);
        return R.success(cart);
    }
    /**
     * 通过某个用户的id删除购物车项
     */
    @DeleteMapping("clean")
    public R<String> clean(){
        log.info("删除购物车");
        long userId = getLonginIdFromSession();
        shoppingCartService.deleteByUserId(userId);
        return R.success("删除成功");
    }
}
