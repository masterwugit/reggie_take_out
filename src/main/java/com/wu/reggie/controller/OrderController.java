package com.wu.reggie.controller;

import com.wu.reggie.common.R;
import com.wu.reggie.entity.Orders;
import com.wu.reggie.entity.Page;
import com.wu.reggie.service.OrderService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/5 - 06 - 05
 * @Description: com.wu.reggie.controller
 * @version: 1.0
 */
@RestController
@RequestMapping("order")
@Slf4j
public class OrderController extends BaseController {
    @Autowired
    private OrderService orderService;
    @PostMapping("submit")
    public R<String> submit(@RequestBody Orders orders){
        orderService.submit(orders,getLonginIdFromSession());
        return R.success("下订单成功");
    }
    /**
     * 查询订单信息
     * @param pageNum
     * @param pageSize
     * @param number
     * @param beginTime
     * @param endTime
     * @return
     */
    @GetMapping("page")
    public R<Page<Orders>> findPage(@RequestParam("page") Integer pageNum, Integer pageSize, Integer number, String beginTime, String endTime){
        Page<Orders> page = orderService.findPage(pageNum, pageSize, number, beginTime, endTime);
        return R.success(page);
    }
}
