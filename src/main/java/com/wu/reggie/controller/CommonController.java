package com.wu.reggie.controller;

import com.wu.reggie.common.R;
import lombok.SneakyThrows;
import org.apache.commons.io.IOUtils;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;
import org.springframework.web.multipart.MultipartFile;

import javax.servlet.ServletOutputStream;
import java.io.File;
import java.io.FileInputStream;
import java.util.UUID;

/**
 * @Author: wu
 * @Date: 2022/6/1 - 06 - 01
 * @Description: com.wu.reggie.controller
 * @version: 1.0
 */
@RestController
@RequestMapping("/common")
public class CommonController extends BaseController {
    //从配置文件中获取上传文件的路径
    @Value("${reggie.path}")
    /**
     * 上传文件
     * MultipartFile方法的参数名：必须与表单文件域的名字一样
     * @return 返回上传的文件名
     */
    private String imgPath;
    @PostMapping("upload")
    @SneakyThrows
    //参数类型必须是MultipartFile，形参名必须有上传组件名字一样，这里是file
    public R<String> upload(MultipartFile file){
        //1. 获取文件的原始文件名
        String originalFilename = file.getOriginalFilename();
        //2. 通过原始文件名获取文件后缀
        String suffix = originalFilename.substring(originalFilename.lastIndexOf("."));
        //3. 通过UUID重新生成文件名, 因为文件名称重复会造成文件覆盖
        String filename = UUID.randomUUID() + suffix;
        file.transferTo(new File(imgPath,filename));
        return R.success(filename);
    }
    /**
     * 显示图片
     * @param name 要显示的图片文件名
     */
    @SneakyThrows
    @GetMapping("download")
    public void download(String name){
        //1. 获取文件输入流
        FileInputStream fileInputStream = new FileInputStream(imgPath + name);
        //2.得到响应输出流
        ServletOutputStream outputStream = response.getOutputStream();
        //3. 使用工具类将输入流复制到输出流
        IOUtils.copy(fileInputStream,outputStream);
        //4.关闭流
        outputStream.close();
        fileInputStream.close();
    }
}
