package com.wu.reggie.controller;

import com.wu.reggie.common.R;
import com.wu.reggie.entity.User;
import com.wu.reggie.service.UserService;
import com.wu.reggie.util.ValidateCodeUtils;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.Map;
import java.util.concurrent.TimeUnit;

/**
 * @Author: wu
 * @Date: 2022/6/4 - 06 - 04
 * @Description: com.wu.reggie.controller
 * @version: 1.0
 */
@Slf4j
@RestController
@RequestMapping("user")
public class UserController extends BaseController {
    @Autowired
    private UserService userService;
    @Autowired
    private RedisTemplate<String, String> redisTemplate;
    /**
     * 发送短信
     */
    @PostMapping("sendMsg")
    public R<String> sendMsg(@RequestBody User user) {
        //1.获取手机号
        String phone = user.getPhone();
        //2.生成验证码
        String code = ValidateCodeUtils.generateValidateCode(4);
        //3.向手机号发送验证码
        //SMSUtils.sendMessage("黑马旅游", "SMS_195723031", phone, code);
        log.info("验证码是：{}", code);
        //4.将验证码放在redis中
        redisTemplate.opsForValue().set(phone,code,1, TimeUnit.MINUTES);
        //session.setAttribute(phone, code);  //手机号为键，验证码为值
        //5.返回信息
        return R.success("发送成功");
    }
    /**
     * 登录和注册
     * 因为user中没有code属性
     */
    @PostMapping("login")
    public R<String> login(@RequestBody Map<String, String> param) {
        //1.获取手机号和验证码
        String phone = param.get("phone");
        String code = param.get("code");
        //2.从会话域中获取验证码与用户提交的验证码去比较
        String redisCode = redisTemplate.opsForValue().get(phone);
        if (redisCode == null) {
            //3.如果验证码过期，登录失败
            return R.error("验证码已过期");
        }else if (!code.equals(redisCode)){
            //3.如果验证码错误，登录失败
            return R.error("验证码错误");
        }
        //4.如果正确，调用业务层来登录(注册)
        User user = userService.login(phone);
        //5.登录操作返回用户对象，如果用户对象不为空，则登录成功
        if (user != null) {
            //登录成功,删除验证码
            redisTemplate.delete(phone);
            //登录成功，将用户的ID放在会话域中
            session.setAttribute("LOGIN_ID", user.getId());
            return R.success("登录成功");
        }
        //6.为空登录失败
        return R.error("登录失败");
    }
    /**
     *用户退出
     */
    @PostMapping("loginout")
    public R<String> loginout(){
        log.info("用户退出");
        session.invalidate();
        return R.success("退出成功");
    }
}
