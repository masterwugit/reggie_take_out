package com.wu.reggie.controller;

import com.wu.reggie.common.R;
import com.wu.reggie.dto.DishDto;
import com.wu.reggie.entity.Dish;
import com.wu.reggie.entity.Page;
import com.wu.reggie.service.DishService;
import io.swagger.annotations.Api;
import io.swagger.annotations.ApiImplicitParam;
import io.swagger.annotations.ApiImplicitParams;
import io.swagger.annotations.ApiOperation;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/1 - 06 - 01
 * @Description: com.wu.reggie.controller
 * @version: 1.0
 */
@RestController
@RequestMapping("dish")
@Slf4j
@Api(tags = "菜品相关接口")
public class DishController {
    @Autowired
    private DishService dishService;
    /**
     * 添加菜品
     * @param dishDto
     */
    @ApiOperation("添加菜品")
    @PostMapping
    public R<String> saveWithFlavor(@RequestBody DishDto dishDto){
        log.info("添加菜品"+dishDto);
        dishService.saveWithFlavor(dishDto);
        return R.success("添加菜品成功");
    }
    /**
     * 分页查询菜品
     * @param pageNum
     * @param pageSize
     * @param name
     */
    @GetMapping("page")
    @ApiOperation("分页查询菜品")
    @ApiImplicitParams({
            @ApiImplicitParam(name = "page",required = true,value = "页码"),
            @ApiImplicitParam(name = "pageSize",required = true,value = "页码的大小"),
            @ApiImplicitParam(name = "name",value = "菜品名"),
    })
    public R<Page<DishDto>> findPage(@RequestParam("page") Integer pageNum, Integer pageSize, String name){
        Page<DishDto> page = dishService.findPage(pageNum, pageSize, name);
        return R.success(page);
    }
    /**
     * 通过id查询菜品和口味信息
     * @param id
     */
    @GetMapping("{id}")
    @ApiOperation("通过id查询菜品和口味信息")
    public R<DishDto> findById(@PathVariable Long id){
        DishDto dishDto = dishService.findById(id);
        return R.success(dishDto);
    }
    /**
     * 更新菜品的信息
     */
    @PutMapping
    @ApiOperation("更新菜品的信息")
    public R<String> update(@RequestBody DishDto dishDto){
        dishService.updateById(dishDto);
        return R.success("修改成功");
    }
    /**
     * 通过Id删除菜品
     * @param ids
     */
    @DeleteMapping
    @ApiOperation("通过Id删除菜品")
    public R<String> delete(Long[] ids){
        dishService.deleteById(ids);
        return R.success("删除成功");
    }
    /**
     * 修改status状态
     * @param status
     * @param ids
     */
    @PostMapping("status/{status}")
    @ApiOperation("修改出售/停售状态")
    public R<String> updateStatus(@PathVariable Integer status,Long[] ids){
        dishService.updateByStatus(status,ids);
        return R.success("修改成功");
    }
    /**
     * 通过categoryId查询菜品
     * @param categoryId
     * @return
     */
    @GetMapping("list")
    @ApiOperation("通过categoryId查询菜品")
    public R<List<Dish>> findByCategoryId(Long categoryId){
        List<Dish> dishes = dishService.findByCategoryId(categoryId);
        return R.success(dishes);
    }
    /**
     * 通过id查询菜品和口味并且是出售状态
     * @param categoryId
     * @param status
     * @return
     */
    @GetMapping("listFlavor")
    @ApiOperation("通过id查询菜品和口味并且是出售状态")
    public R<List<DishDto>> findDishWithFlavor(Long categoryId,Integer status){
        List<DishDto> dishDto = dishService.findDishWithFlavor(categoryId, status);
        return R.success(dishDto);
    }
}
