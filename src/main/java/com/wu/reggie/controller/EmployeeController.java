package com.wu.reggie.controller;

import com.wu.reggie.common.R;
import com.wu.reggie.entity.Employee;
import com.wu.reggie.entity.Page;
import com.wu.reggie.service.EmployeeService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.io.PrintWriter;

/**
 * @Author: wu
 * @Date: 2022/5/27 - 05 - 27
 * @Description: com.wu.reggie.controller
 * @version: 1.0
 */

/**
 * 员工控制器
 * 持久层返回类型：
 * 1. 增删改返回整数或void类型
 * 2. 查询1条记录，返回一个对象
 * 3. 查询多条记录，返回集合List
 */
@RestController
@RequestMapping("employee")
@Slf4j
public class EmployeeController extends BaseController{
    @Autowired
    private EmployeeService employeeService;
    @PostMapping("login")
    public R<Employee> login(@RequestBody Employee employee){
        //1. 调用业务层实现登录，返回R<Employee>对象
        R<Employee> result = employeeService.login(employee);
        //2. 如果代码为1，则将employeeId放到会话域中，起名为LOGIN_ID
        if (result.getCode()== 1) {
            //getData()指的是查询出来的员工对象
            session.setAttribute("LOGIN_ID", result.getData().getId());
        }
        return result;
    }
    /**
     * 退出功能
     */
    @PostMapping("logout")
    public R<String> logout(){
        log.info("用户id:{}",session.getAttribute("LOGIN_ID"));
        session.invalidate();
        return R.success("退出成功");
    }
    /**
     * 查询员工一页数据
     * @return
     */
    @GetMapping("page")
    public R<Page<Employee>> page(Integer page,Integer pageSize,String name){
        Page<Employee> pages = employeeService.findByName(page, pageSize, name);
        return R.success(pages);
    }
    /**
     * 添加员工
     */
    @PostMapping
    public R<String> save(@RequestBody Employee employee){
//       Long id = getLonginIdFromSession();
//       employee.setCreateUser(id);
//       employee.setUpdateUser(id);
        employeeService.save(employee);
        return R.success("保存员工成功");
    }
    //修改员工的状态
    @PutMapping
    public R<String> update(@RequestBody Employee employee){
        long loginid = (long) session.getAttribute("LOGIN_ID");
//        employee.setUpdateUser(loginid);
        employeeService.update(employee);
        return R.success("修改成功");
    }
    //通过Id查询员工信息(数据回显)
    @GetMapping("{id}")
    public R<Employee> findById(@PathVariable Long id){
        log.info("查询员工id:{}",id);
        Employee employee = employeeService.findById(id);
        return R.success(employee);
    }

}
