package com.wu.reggie.controller;

import com.wu.reggie.common.R;
import com.wu.reggie.dto.SetmealDto;
import com.wu.reggie.entity.Page;
import com.wu.reggie.entity.Setmeal;
import com.wu.reggie.service.SetmealService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/6/2 - 06 - 02
 * @Description: com.wu.reggie.controller
 * @version: 1.0
 */
@RestController
@RequestMapping("setmeal")
@Slf4j
public class SetmealController {
    @Autowired
    private SetmealService setmealService;
    @PostMapping
    public R<String> save(@RequestBody SetmealDto setmealDto){
        log.info("保存套餐{}",setmealDto);
        setmealService.saveWithDish(setmealDto);
        return R.success("保存成功");
    }
    /**
     * 分页查询
     * @param pageNum
     * @param pageSize
     * @param name
     * @return
     */
    @GetMapping("page")
    public R<Page<SetmealDto>> findPage(@RequestParam("page") Integer pageNum,Integer pageSize,String name){
        Page<SetmealDto> page = setmealService.findPage(pageNum, pageSize, name);
        return R.success(page);
    }
    /**
     * 根据ids删除多条数据和套餐菜品关系表
     * @param ids
     */
    @DeleteMapping
    public R<String> delete(Long[] ids){
        log.info("删除菜品的id{}",ids);
        setmealService.delete(ids);
        return R.success("删除成功");
    }
    /**
     * 批量启售,停售
     * @param status
     * @param ids
     */
    @PostMapping("status/{status}")
    public R<String> updateStatus(@PathVariable int status,Long[] ids){
        setmealService.updateStatus(status,ids);
        return R.success("修改成功");
    }
    /**
     * 通过id查询套餐并且是出售状态
     * @param categoryId
     * @param status
     * @return
     */
    @GetMapping("list")
    public R<List<Setmeal>> findSetmealByStatus(Long categoryId,Integer status){
        List<Setmeal> setmeal = setmealService.findSetmealByStatus(categoryId, status);
        return R.success(setmeal);
    }
}
