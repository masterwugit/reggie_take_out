package com.wu.reggie;

import lombok.extern.slf4j.Slf4j;
import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cache.annotation.EnableCaching;
import org.springframework.transaction.annotation.EnableTransactionManagement;

/**
 * @Author: wu
 * @Date: 2022/5/27 - 05 - 27
 * @Description: com.wu.reggie
 * @version: 1.0
 */
@SpringBootApplication
@MapperScan("com.wu.reggie.mapper")
@EnableTransactionManagement
@Slf4j
@EnableCaching
public class ReggieApplication {
    public static void main(String[] args) {
        log.info("服务器启动");
        SpringApplication.run(ReggieApplication.class,args);
    }
}
