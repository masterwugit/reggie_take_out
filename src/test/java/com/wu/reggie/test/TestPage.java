package com.wu.reggie.test;

import com.github.pagehelper.Page;
import com.github.pagehelper.PageHelper;
import com.github.pagehelper.PageInfo;
import com.wu.reggie.entity.Employee;
import com.wu.reggie.mapper.EmployeeMapper;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

/**
 * @Author: wu
 * @Date: 2022/5/29 - 05 - 29
 * @Description: com.wu.reggie.test
 * @version: 1.0
 */
@SpringBootTest
public class TestPage {
    @Autowired
    private  EmployeeMapper employeeMapper;
    @Test
    public void findById(){
        PageHelper.startPage(1, 5);
        List<Employee> employees = employeeMapper.findByName("");
        PageInfo<Employee> PageInfo = new PageInfo<>(employees);
        System.out.println("总记录数: "+PageInfo.getTotal());
        System.out.println("总页数: "+PageInfo.getPages());
        System.out.println("每页记录数: "+PageInfo.getPageSize());
        System.out.println("每页记录的数据: ");
        PageInfo.getList().forEach(System.out::println);



    }
}
